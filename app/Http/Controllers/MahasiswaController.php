<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{

   public function index()
   {
      $data_mahasiswa = \App\Mahasiswa::paginate(10);
      return view('mahasiswa.index', ['data_mahasiswa' => $data_mahasiswa], compact('data_mahasiswa'));
   }
   public function indexApi()
   {
      $data_mahasiswa = \App\Mahasiswa::paginate(10);
      return $data_mahasiswa;
   }
   public function create(Request $request)
   {
      \App\Mahasiswa::create($request->all());
      return redirect('/mahasiswa')->with('sukses', 'Data Berhasil Ditambah');
   }
   public function createApi(Request $request)
   {
      \App\Mahasiswa::create($request->all());
      return [
         'message' => 'Berhasil di simpan.'
      ];
   }
   public function edit($id)
   {
      $mahasiswa = \App\Mahasiswa::find($id);
      return view('mahasiswa/edit', ['mahasiswa' => $mahasiswa]);
   }
   public function update(Request $request, $id)
   {
      $mahasiswa = \App\Mahasiswa::find($id);
      $mahasiswa->update($request->all());
      return redirect('/mahasiswa')->with('sukses', 'Data berhasil diupdate');
   }
   public function updateApi(Request $request, $id)
   {
      $mahasiswa = \App\Mahasiswa::find($id);
      $mahasiswa->update($request->all());
      return  [
         'message' => 'Berhasil di Edit.'
      ];
   }
   public function delete($id)
   {
      $mahasiswa = \App\Mahasiswa::find($id);
      $mahasiswa->delete();
      return redirect('/mahasiswa')->with('sukses', 'Data berhasil dihapus');
   }

   public function deleteApi($id)
   {
      $mahasiswa = \App\Mahasiswa::find($id);
      $mahasiswa->delete();
      return [
         'message' => 'Berhasil di Edit.'
      ];
   }
}
