<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;

class JurusanController extends Controller
{
    public function index()
    {
        $data_jurusan = Jurusan::paginate(10);
        return view('jurusan.index', ['data_jurusan' => $data_jurusan], compact('data_jurusan'));
    }

    public function indexApi()
    {
        $data_jurusan = Jurusan::paginate(10);
        return $data_jurusan;
    }
    function create()
    {
        Jurusan::create([
            'nama_jurusan' => request()->input('nama'),
            'nama_kepala_jurusan' => request()->input('nama_kepala')
        ]);
        return redirect('/jurusan')->with('sukses', 'Data Berhasil Ditambah');
    }
    function createApi()
    {
        Jurusan::create([
            'nama_jurusan' => request()->input('nama'),
            'nama_kepala_jurusan' => request()->input('nama_kepala')
        ]);
        return [
            'message' => 'Berhasil di simpan.'
        ];
    }
    public function edit($id)
    {
        $jurusan = Jurusan::find($id);
        return view('jurusan/edit', ['jurusan' => $jurusan]);
    }
    
    public function update(Request $request, $id)
    {
        $jurusan = Jurusan::find($id);
        $jurusan->update($request->all());
        return redirect('/jurusan')->with('sukses', 'Data berhasil diupdate');
    }
    public function updateApi(Request $request, $id)
    {
        $jurusan = Jurusan::find($id);
        $jurusan->update($request->all());
        return [
            'message' => 'Berhasil di edit.'
        ];
    }
    public function delete($id)
    {
        $jurusan = Jurusan::find($id);
        $jurusan->delete();
        return redirect('/jurusan')->with('sukses', 'Data berhasil dihapus');
    }
    public function deleteApi($id)
    {
        $jurusan = Jurusan::find($id);
        $jurusan->delete();
        return [
            'message' => 'Berhasil di hapus.'
        ];
    }
}
