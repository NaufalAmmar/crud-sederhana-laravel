<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    //
    protected $table='mahasiswa';
    protected $fillable=['id','Nama','Jenis_Kelamin','Jurusan','Alamat'];
}
