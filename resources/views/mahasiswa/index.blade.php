@extends('layout.master')

@section('content')
		@if(session('sukses'))
			<div class="alert alert-success" role="alert">
				{{session('sukses')}}
			</div>
		@endif	
		<div class="row">
			<div class="col-6">
				<h1>Data Mahasiswa </h1>
			</div>
			<div class="col-6">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
				  Tambah Data Mahasiswa
				</button>

			</div>
			
			<table class="table table-hover">
				<tr>
					<th>id</th>
					<th>Nama</th> 
					<th>Jenis Kelamin</th>
					<th>Jurusan</th>
					<th>Alamat</th>
					<th>Aksi</th>
				</tr>
			    @foreach($data_mahasiswa as $mahasiswa)
				<tr>
					<td>{{$mahasiswa->id}}</td>
					<td>{{$mahasiswa->Nama}}</td>
					<td>{{$mahasiswa->Jenis_Kelamin}}</td>
					<td>{{$mahasiswa->Jurusan}}</td>
					<td>{{$mahasiswa->Alamat}}</td>

					<td><a href="/mahasiswa/{{$mahasiswa->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
					<button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDel{{$mahasiswa->id}}">Hapus</button>
				</td>

				</tr>
				
<!-- modal delete -->
<div class="modal fade" id="ModalDel{{$mahasiswa->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Mahasiswa</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<h1>Hapus Data ?</h1>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <a href="/mahasiswa/{{$mahasiswa->id}}/delete" class="btn btn-warning">Hapus</a>
				        </form>
				      </div>
				    </div>
				  </div>
				</div>
				

				@endforeach
			</table>
			{{$data_mahasiswa->render()}}
		</div>
	 </div>

	 <!-- Modal create -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Input Data Mahasiswa</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form action="/mahasiswa/create" method="POST">
				        	{{csrf_field()}}
						  <div class="form-group">
						    <label for="exampleInputEmail1">id</label>
						    <input name="id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="id">
						  </div>
						   <div class="form-group">
						    <label for="exampleInputEmail1">Nama</label>
						    <input name="Nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama">
						  </div>
						  <div class="form-group">
						    <label for="exampleFormControlSelect1">Pilih Jenis Kelamin</label>
						    <select name="Jenis_Kelamin" class="form-control" id="exampleFormControlSelect1">
						      <option value="L">Laki-Laki</option>
						      <option value="P">Perempuan</option>
						    </select>
						  </div>
						   <div class="form-group">
						    <label for="exampleInputEmail1">Jurusan</label>
						    <input name="Jurusan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Jurusan">
						  </div>
						  <div class="form-group">
						    <label for="exampleFormControlTextarea1">Alamat</label>
						    <textarea  name="Alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
						  </div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Submit</button>
				        </form>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
@endsection