@extends('layout.master')

@section('content')
		<h1>Edit data mahasiswa</h1>
		@if(session('sukses'))
			<div class="alert alert-success" role="alert">
				{{session('sukses')}}
			</div>
		@endif	
		<div class="row">
			<div class="col-lg-12">
				
				
			<form action="/mahasiswa/{{$mahasiswa->id}}/update" method="POST">
				        	{{csrf_field()}}
						   <div class="form-group">
						    <label for="exampleInputEmail1">Nama</label>
						    <input name="Nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama" value="{{$mahasiswa->Nama}}">
						  </div>
						
						  <div class="form-group">
						    <label for="exampleFormControlSelect1">Pilih Jenis Kelamin</label>
						    <select name="Jenis_Kelamin" class="form-control" id="exampleFormControlSelect1">
						      <option value="L" @if($mahasiswa->Jenis_Kelamin == 'L')selected @endif>Laki-Laki</option>
						      <option value="P" value="L" @if($mahasiswa->Jenis_Kelamin == 'P')selected @endif>Perempuan</option>
						    </select>
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Jurusan</label>
						    <input name="Jurusan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Jurusan" value="{{$mahasiswa->Jurusan}}">
						  </div>
						  <div class="form-group">
						    <label for="exampleFormControlTextarea1">Alamat</label>
						    <textarea  name="Alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$mahasiswa->Alamat}}	</textarea>
						  </div>
						  <button type="submit" class="btn btn-warning">Update</button>
				        </form>
				        </div>
		</div>
	 </div>
@endsection