@extends('layout.master')

@section('content')
<h1>Edit data jurusan</h1>
@if(session('sukses'))
<div class="alert alert-success" role="alert">
	{{session('sukses')}}
</div>
@endif
<div class="row">
	<div class="col-lg-12">


		<form action="/jurusan/{{$jurusan->id}}/update" method="POST">
			{{csrf_field()}}
			<div class="form-group">
				<label for="exampleInputEmail1">Nama Jurusan</label>
				<input name="nama_jurusan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama" value="{{$jurusan->nama_jurusan}}">
			</div>


			<div class="form-group">
				<label for="exampleInputEmail1">Nama Kepala Jurusan</label>
				<input name="nama_kepala_jurusan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Jurusan" value="{{$jurusan->nama_kepala_jurusan}}">
			</div>

			<button type="submit" class="btn btn-warning">Update</button>
		</form>
	</div>
</div>
</div>
@endsection