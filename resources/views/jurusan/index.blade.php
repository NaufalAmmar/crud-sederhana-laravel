@extends('layout.master')

@section('content')
		@if(session('sukses'))
			<div class="alert alert-success" role="alert">
				{{session('sukses')}}
			</div>
		@endif	
		<div class="row">
			<div class="col-6">
				<h1>Data Jurusan </h1>
			</div>
			<div class="col-6">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
				  Tambah Data Jurusan
				</button>

			</div>
			
			<table class="table table-hover">
				<tr>
					<th>id</th>
					<th>Nama Jurusan</th> 
					<th>Nama Kepala Jurusan</th> 				
					<th>Aksi</th>
				</tr>
			    @foreach($data_jurusan as $jurusan)
				<tr>
					<td>{{$jurusan->id}}</td>
					<td>{{$jurusan->nama_jurusan}}</td>
					<td>{{$jurusan->nama_kepala_jurusan}}</td>				

					<td><a href="/jurusan/{{$jurusan->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
					<button type="button" class="btn btn-danger btn-sm " data-toggle="modal" data-target="#ModalDel{{$jurusan->id}}">Hapus</button>
				</td>

				</tr>

<!-- modal delete -->
				<div class="modal fade" id="ModalDel{{$jurusan->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Jurusan</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<h1>Hapus Data ?</h1>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <a href="/jurusan/{{$jurusan->id}}/delete" class="btn btn-warning">Hapus</a>
				        </form>
				      </div>
				    </div>
				  </div>
				</div>
				@endforeach
			</table>
			{{$data_jurusan->render()}}
		</div>
	 </div>
				

	 <!-- Modal create -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Input Data Jurusan</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form action="/jurusan/create" method="POST">
				        	{{csrf_field()}}						
						   <div class="form-group">
						    <label for="exampleInputEmail1">Nama Jurusan</label>
						    <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama">
						  </div>	
						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Kepala Jurusan</label>
						    <input name="nama_kepala" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama">
						  </div>					
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-primary">Submit</button>
				        </form>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
@endsection