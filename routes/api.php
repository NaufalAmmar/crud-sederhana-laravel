<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/mahasiswa', 'MahasiswaController@indexApi');
Route::post('/mahasiswa/create', 'MahasiswaController@createApi');
Route::post('/mahasiswa/{id}/update', 'MahasiswaController@updateApi');
Route::get('/mahasiswa/{id}/delete', 'MahasiswaController@deleteApi');

Route::get('/jurusan', 'JurusanController@indexApi');
Route::post('/jurusan/create', 'JurusanController@createApi');
Route::post('/jurusan/{id}/update', 'JurusanController@updateApi');
Route::get('/jurusan/{id}/delete', 'JurusanController@deleteApi');